module testrunner

import StdArray
import StdBool
import StdFile
from StdFunc import flip, o
import StdList
import StdMisc
import StdString
import StdTuple

from Control.Monad import class Monad, mapM
import Data.Error
from Data.Func import $, mapSt, seqSt
import Data.Functor
import Data.GenDiff
import Data.GenEq
import Data.List
import Data.Maybe
import Data.Tuple
import System.CommandLine
import System.FilePath
import System.Options
import System.Process
import System.Time
import Testing.Options
import Testing.TestEvents
from Text import class Text(join,lpad,ltrim,replaceSubString,split,trim,concat),
	instance Text String, concat4, concat5, <+
import Text.GenJSON
import Text.GenPrint
import Text.Language

import Testing.JUnitExport
import Testing.Util
import Util.ProcessOutput
import Util.TestSelection

:: MessageType = MT_Started | MT_Passed | MT_Failed | MT_Skipped | MT_Lost
:: OutputFormat = OF_HumanReadable | OF_JSON
:: Strategy = S_Default | S_FailedFirst
:: ParallelJob = ParallelJob !Int !Int // index of total; index starts at 1

messageType :: TestEvent -> MessageType
messageType (StartEvent _) = MT_Started
messageType (EndEvent ee) = case ee.event of
	Passed   -> MT_Passed
	Failed _ -> MT_Failed
	Skipped  -> MT_Skipped

derive gEq MessageType; instance == MessageType where (==) a b = a === b

:: Options =
	{ test_options          :: !TestOptions
	, strategy              :: !Strategy
	, output_format         :: !OutputFormat
	, no_color              :: !Bool
	, output_junit_file     :: !?FilePath
	, hide                  :: ![MessageType]
	, stop_on_first_failure :: !Bool
	, parallel_job          :: !?ParallelJob
	}

derive gDefault MessageType, Options, OutputFormat, Strategy, ParallelJob

optionDescription :: Option Options
optionDescription = WithHelp True $ Options
	[ Shorthand "-s" "--strategy" $ AddHelpLines
		[ "default       Order of the --run parameters"
		, "failed-first  First run tests that failed last time; if they passed continue with the rest"
		] $ Option
		"--strategy"
		(\s opts -> case s of
			"default"      -> Ok {opts & strategy=S_Default}
			"failed-first" -> Ok {opts & strategy=S_FailedFirst}
			s              -> Error ["Unknown strategy '" <+ s <+ "'"])
		"STRATEGY"
		"The test order strategy:"
	, Shorthand "-f" "--output-format" $ Option
		"--output-format"
		(\f opts -> case f of
			"json"  -> Ok {opts & output_format=OF_JSON}
			"human" -> Ok {opts & output_format=OF_HumanReadable}
			f       -> Error ["Unknown output format '" <+ f <+ "'"])
		"FMT"
		"The output format (json,human)"
	, Flag "--no-color"
		(\opts -> Ok {opts & no_color = True})
		"Do not use ANSI escape codes for colored output (only used with --output-format human)"
	, Option
		"--junit"
		(\f opts -> Ok {opts & output_junit_file= ?Just f})
		"FILE"
		"Output test results in JUnit XML format to FILE"
	, Shorthand "-H" "--hide" $ Option
		"--hide"
		(\mts opts -> (\mts -> {opts & hide=mts}) <$> (mapM parseMT $ split "," mts))
		"TYPES"
		"Message types that should be hidden (start,pass,fail,skip,lost)"
	, Shorthand "-F" "--stop-on-first-failure" $ Flag
		"--stop-on-first-failure"
		(\opts -> Ok {opts & stop_on_first_failure = True})
		"Stop after the first test failed"
	, Option
		"--parallel-job"
		(\j opts -> (\j -> {opts & parallel_job= ?Just j}) <$> parseParallelJob j)
		"INDEX/TOTAL"
		"Run only tests INDEX modulo TOTAL (useful to parallelise tests over TOTAL runners)"
	, Biject (\r->r.test_options) (\old r -> {old & test_options=r}) testOptionDescription
	]
where
	parseMT :: String -> MaybeError [String] MessageType
	parseMT "start" = Ok MT_Started
	parseMT "pass"  = Ok MT_Passed
	parseMT "fail"  = Ok MT_Failed
	parseMT "skip"  = Ok MT_Skipped
	parseMT "lost"  = Ok MT_Lost
	parseMT s       = Error ["Unknown message type '" <+ s <+ "'"]

	parseParallelJob :: !String -> MaybeError [String] ParallelJob
	parseParallelJob s
		| isNone slash = error
		# (?Just slash) = slash
		# index = toInt (s % (0,slash-1))
		# total = toInt (s % (slash+1,size s-1))
		| index == 0 || total == 0
			= error
			= Ok (ParallelJob index total)
	where
		slash = listToMaybe [i \\ i <- [0..] & c <-: s | c=='/']
		error = Error ["--parallel-job should be given as INDEX/TOTAL, where INDEX and TOTAL are positive integers"]

:: SubTestRun
	= JustRun           !TestRun // Run all tests
	| Only    ![String] !TestRun // Only run specific tests (by passing their names via the `--run` argument)
	| Without ![String] !TestRun // Skip specific tests (by passing their names via the `--skip` argument)

Start w
// Parse command line arguments
# ([prog:args],w) = getCommandLine w
# opts = parseOptions optionDescription args gDefault{|*|}
| isError opts
	= exit (join "\n" $ fromError opts) w
# opts = fromOk opts
| opts.test_options.list
	# (io,w) = stdio w
	# io = foldl (<<<) io [r.TestRun.name +++ "\n" \\ r <- opts.test_options.runs]
	# (_,w) = fclose io w
	= w
| isJust opts.parallel_job && opts.strategy=:S_FailedFirst
	= exit "--parallel-job and --strategy failed-first are incompatible" w
// Run tests
# (runs,w) = makeRuns opts w
# (_,rrs,w) = seqSt (runIteration opts) runs (True, [], w)
// Save results
# (ok,f,w) = fopen ".ctest-results.json" FWriteText w
# w = case ok of
	True
		# f = f <<< toJSON (mergeResults rrs)
		# (_,w) = fclose f w
		-> w
	False
		# (_,w) = fclose (stderr <<< "Failed to save .ctest-results.json\n") w
		-> w
// JUnit export
# w = case opts.output_junit_file of
	?None
		-> w
	?Just junitf
		# (ok,f,w) = fopen junitf FWriteText w
		| not ok
			# (_,w) = fclose (stderr <<< "Failed to open '" <<< junitf <<< "', skipping JUnit export\n") w
			-> w
		// NB: no mergeResults here. Having a JUnit export does not really make
		// sense with --rerun-failed, so we can assume that all TestRun names
		// are unique.
		# f = f <<< resultsToJUnitExport rrs
		# (_,w) = fclose f w
		-> w
= w
where
	runIteration :: !Options !SubTestRun !*(!Bool, ![RunResult], !*World) -> *(!Bool, ![RunResult], !*World)
	runIteration opts test (continue,rrs,w)
	| not continue = (False, reverse rrs, w)
	# (r,w) = run opts test w
	= (not $ opts.stop_on_first_failure && r.result=:Failed _, [r:rrs], w)

	exit :: String *World -> *World
	exit error w = snd $ fclose (stderr <<< error <<< "\n") $ setReturnCode 1 w

	makeRuns :: !Options !*World -> (![SubTestRun], !*World)
	makeRuns {strategy=S_Default,parallel_job=(?Just (ParallelJob idx total)),test_options={runs}} w =
		makeRunsForParallelJob idx total runs w
	makeRuns {strategy=S_Default,parallel_job= ?None,test_options={runs}} w =
		(map JustRun runs, w)
	makeRuns {strategy=S_FailedFirst,test_options={runs}} w
		# (ok,f,w) = fopen ".ctest-results.json" FReadText w
		| not ok
			= (map JustRun runs, w)
		# (l,f) = freadline f
		# (_,w) = fclose f w
		= case fromJSON (fromString l) of
			?None ->
				(map JustRun runs, w)
			?Just res ->
				(subruns, w)
			with
				subruns =
					map (uncurry (flip Only)) failed_children ++
					map JustRun failed ++
					map (uncurry (flip Without)) failed_children ++
					map JustRun not_failed

				failed_children = [(run, map fst cs) \\ {run,result=Failed (?Just (FailedChildren cs))} <- res]
				failed = [run \\ {run,result=Failed fr} <- res | not (fr=:(?Just (FailedChildren _)))]
				not_failed = [run \\ {run,result=res} <- res | not (res=:(Failed _))]

	/* Prepares test runs based on parallel execution.
	 * @param The index.
	 * @param The total.
	 * @param The total list of tests to run.
	 * @result The selected tests.
	 */
	makeRunsForParallelJob :: !Int !Int ![TestRun] !*World -> (![SubTestRun], !*World)
	makeRunsForParallelJob idx total runs w = appFst catMaybes $ mapSt pickTests runs w
	where
		pickTests :: !TestRun !*World -> (!?SubTestRun, !*World)
		pickTests run w
			# (names, w) = list run w
			# names = pickTestsForParallelJob idx total names
			= (if (isEmpty names) ?None (?Just (Only names run)), w)

list :: !TestRun !*World -> *(![String], !*World)
list r w
# (h,w) = runProcessIO r.TestRun.name ["--list"] ?None w
| isError h = ([], w)
# (h,io) = fromOk h
# (c,w) = waitForProcess h w
| isError c = ([], w)
# (s,w) = readPipeBlocking io.stdOut w
| isError s = ([], w)
# (_,w) = closeProcessIO io w
= (filter (not o (==) 0 o size) $ map trim $ split "\n" (fromOk s), w)

run :: !Options !SubTestRun !*World -> *(!RunResult, !*World)
run opts r w
# (start,w) = nsTime w
# (io,w) = stdio w
# (timestamp, w) = humanReadableTimeStamp w
# io = emit (StartEvent {StartEvent | name=name, location= ?None}) timestamp io
	with name = (case r of JustRun r   -> r; Only _ r    -> r; Without _ r -> r).TestRun.name
# (extra_opts,r,w) = case r of
	JustRun r       -> (?Just [], r, w)
	Only names    r -> (?Just ["--run":intersperse "--run" names], r, w)
	Without names r -> (\(all,w) -> (case difference all names of
		[] -> ?None
		_  -> ?Just ["--skip":intersperse "--skip" names], r, w)) $ list r w
| isNone extra_opts
	# (timestamp, w) = humanReadableTimeStamp w
	# io = emit (EndEvent
		{ emptyEndEvent
		& name     = r.TestRun.name
		, event    = Passed
		, message  = "No remaining tests"
		}) timestamp io
	= return start Passed [] r io w
# extra_opts = fromJust extra_opts
# (h,w) = runProcessIO r.TestRun.name (r.options ++ extra_opts) ?None w
| isError h
	# (err,msg) = fromError h
	# event = Failed ?None
	# (timestamp, w) = humanReadableTimeStamp w
	# io = emit (EndEvent
		{ emptyEndEvent
		& name     = r.TestRun.name
		, event    = event
		, message  = "Failed to execute " <+ r.TestRun.name <+ " (" <+ err <+ "; " <+ msg <+ ")"
		}) timestamp io
	= return start event [] r io w
# (h,pio) = fromOk h
# w = snd $ fclose io w
= redirect start {lines=[], rest=""} h pio r w
where
	redirect :: !Timespec !*ProcessOutput !ProcessHandle !ProcessIO !TestRun *World -> *(!RunResult, !*World)
	redirect start output h pio r w
	# (io,w) = stdio w
	// Check child output
	# (continue,output,io,w) = readPipes output pio io w
	| isError continue = return start (Failed ?None) [] r io w
	| continue=:(Ok False)
		# results = map (fromJSON o fromString) $ filter ((<>) "") output.lines
		# events = map fromJust results
		# (lost,ee) = collectEvents events
		# (timestamp, w) = humanReadableTimeStamp w
		# io = foldl (\f l -> emit l timestamp f) io lost
		= return start ee.event [ee \\ EndEvent ee <- events ++ lost] r io w
	// Check if child has terminated
	# (t,w) = checkProcess h w
	| isError t
		# (err,msg) = fromError t
		# event = Failed ?None
		# (timestamp, w) = humanReadableTimeStamp w
		# io = emit (EndEvent
			{ emptyEndEvent
			& name     = r.TestRun.name
			, event    = event
			, message  = "Failed to check on child process (" <+ err <+ "; " <+ msg <+ ")"
			}) timestamp io
		= return start event [] r io w
	# rcode = fromOk t
	// Check return code
	| isJust rcode
		# (_,output,io,w) = readPipes output pio io w
		# results = map (fromJSON o fromString) $ filter ((<>) "") output.lines
		| any isNone results
			# event = Failed ?None
			# (timestamp, w) = humanReadableTimeStamp w
			# io = emit (EndEvent
				{ emptyEndEvent
				& name     = r.TestRun.name
				, event    = event
				, message  = join "\n    "
					["Failed to read child messages:": [outp \\ outp <- output.lines & ?None <- results]]
				}) timestamp io
			= return start event [] r io w
		# events = map fromJust results
		# (lost,ee) = collectEvents events
		# events = events ++ lost
		# (timestamp, w) = humanReadableTimeStamp w
		# io = foldl (\f l -> emit l timestamp f) io lost
		| fromJust rcode <> 0
			# msg = "Child process exited with " <+ fromJust rcode <+ "."
			# ee & message = ltrim (ee.message +++ "\n" +++ msg)
			# ee & event = case ee.event of
				Passed -> Failed (?Just (CustomFailReason msg))
				_      -> ee.event
			# (timestamp, w) = humanReadableTimeStamp w
			# io = emit (EndEvent ee) timestamp io
			= return start ee.event [ee \\ EndEvent ee <- events] r io w
		# (timestamp, w) = humanReadableTimeStamp w
		# io = emit (EndEvent ee) timestamp io
		= return start ee.event [ee \\ EndEvent ee <- events] r io w
	# w = snd $ fclose io w
	= redirect start output h pio r w
	where
		collectEvents :: [TestEvent] -> ([TestEvent], EndEvent)
		collectEvents tes =
			( [EndEvent {emptyEndEvent & name=se.StartEvent.name, location=se.StartEvent.location, event=Failed (?Just Crashed)} \\ se <- lost],
			{ emptyEndEvent
			& name     = r.TestRun.name
			, event    = if (isEmpty failed && isEmpty lost) Passed
				(Failed $ ?Just $ FailedChildren $
					[(name, fr) \\ EndEvent {name,event=Failed fr} <- failed] ++
					[(l.StartEvent.name, ?Just Crashed) \\ l <- lost])
			, message =
				pluralisen English (length passed)  "test" <+ " passed, " <+
				pluralisen English (length failed)  "test" <+ " failed, " <+
				pluralisen English (length skipped) "test" <+ " skipped and " <+
				pluralisen English (length lost)    "test" <+ " lost."
			})
		where
			passed  = filter (\te -> te=:(EndEvent {event=Passed}))   tes
			failed  = filter (\te -> te=:(EndEvent {event=Failed _})) tes
			skipped = filter (\te -> te=:(EndEvent {event=Skipped}))  tes
			lost = [se \\ StartEvent se <- tes
				| not
					$ any (\(EndEvent ee) -> se.StartEvent.name == ee.EndEvent.name)
					$ passed ++ failed ++ skipped]

		readPipes :: !*ProcessOutput !ProcessIO !*File !*World -> *(!MaybeOSError Bool, !*ProcessOutput, !*File, !*World)
		readPipes output pio io w
		# (outerr,w) = readPipeBlockingMulti [pio.stdOut, pio.stdErr] w
		| isError outerr
			# oserr=:(err,msg) = fromError outerr
			# event = Failed ?None
			# (timestamp, w) = humanReadableTimeStamp w
			# io = emit (EndEvent
				{ emptyEndEvent
				& name     = r.TestRun.name
				, event    = event
				, message  = "Failed to read child process IO (" <+ err <+ "; " <+ msg <+ ")"
				}) timestamp io
			= (Error oserr, output, io, w)
		# [out,err:_] = fromOk outerr
		# (timestamp, w) = humanReadableTimeStamp w
		# (output,(continue,io)) = append out
			(\s (continue,io)
				| not continue && opts.stop_on_first_failure
					-> (continue, io)
					-> case fromJSON $ fromString s of
						?None    = (True, io)
						?Just ev
							= (not $ ev=:(EndEvent {event=Failed _}), emit ev timestamp io))
			output
			(True, io)
		# continue = continue || not opts.stop_on_first_failure
		# w = snd $ fclose (stderr <<< err) w
		= (Ok continue, output, io, w)

	emit :: !TestEvent !String *File -> *File
	emit ev timestamp io
	| isMember (messageType ev) opts.hide = io
	| otherwise = case opts.output_format of
		OF_JSON          -> io <<< toJSON ev <<< "\n"
		OF_HumanReadable -> io <<< humanReadable ev timestamp <<< "\n"
	where
		humanReadable :: TestEvent !String -> String
		humanReadable (StartEvent se) timestamp = concat4 "[" timestamp "] Started:  " se.StartEvent.name
		humanReadable (EndEvent ee) timestamp =
			concat
				[ "["
				, timestamp
				, "] "
				, event
				, ee.EndEvent.name
				, time
				, diff
				]
		where
			event = case ee.event of
				Passed   -> if opts.no_color "Passed:   " "\033[0;32mPassed\033[0m:   "
				Failed _ -> if opts.no_color "Failed:   " "\033[0;31mFailed\033[0m:   "
				Skipped  -> if opts.no_color "Skipped:  " "\033[0;33mSkipped\033[0m:  "
			time = maybe "" printTime ee.EndEvent.time
			where
				printTime ms = concat5 " (" (toString (ms/1000)) "." (lpad (toString (ms rem 1000)) 3 '0') "s)"
			diff = case ee.event of
				Failed (?Just r) -> case r of
					FailedAssertions fas -> "\n  Failed assumptions:\n    " +++
						replaceSubString "\n" "\n    "
							(replaceSubString "\t" "  " $ join "\n" $ map printFA fas)
					CounterExamples ces  -> "\n  Counter-examples:\n  - " +++
						join "\n  - " (map (replaceSubString "\n" "\n    " o printCE) ces)
					FailedChildren fcs   -> "\n  Children tests failed: " +++ join ", " (map fst fcs)
					Crashed              -> "\n  Crashed"
					CustomFailReason r   -> "\n  " +++ r
				Failed ?None -> "\n  " +++ ee.message
				_ -> ""
			where
				printFA :: FailedAssertion -> String
				printFA (ExpectedRelation x rel y) = "Expected " +++ toString rel +++ " on:\n" +++ case rel of
					Eq -> diffToConsole $ gDiff{|*|} x y
					_  -> toString x +++ "\n" +++ toString y

				printCE :: CounterExample -> String
				printCE ce = join " " (map toString ce.counterExample) +++ case ce.failedAssertions of
					[]  -> ""
					fas -> ":\n" +++ join "\n" (map printFA ce.failedAssertions)

	humanReadableTimeStamp :: !*World -> (!String, !*World)
	humanReadableTimeStamp w = appFst humanReadableTimeFormat $ localTime w
	where
		humanReadableTimeFormat :: !Tm -> String
		humanReadableTimeFormat tm = strfTime "%Y-%m-%dT%H:%M:%S" tm

	return :: !Timespec !EndEventType ![EndEvent] !TestRun !*File !*World -> *(!RunResult, !*World)
	return start eet children r io w
	# (end,w) = nsTime w
	  elapsed = end - start
	  elapsed_ms = elapsed.tv_sec * 1000 + elapsed.tv_nsec / 1000000
	# (_,w) = fclose io w
	# w = case eet of
		Failed _ -> setReturnCode 1 w
		_        -> w
	= ({run=r, result=eet, children=children, time=elapsed_ms}, w)
