implementation module Testing.Util

import StdEnv

import Data.Func
import Data.Functor
import Data.GenDiff
import Data.List
import Data.Maybe
import Testing.Options
import Testing.TestEvents
import Text.GenJSON
import Text.GenParse
import Text.GenPrint

derive JSONEncode RunResult, TestRun, EndEventType
derive JSONDecode RunResult, TestRun, EndEventType

instance <<< RunResult where (<<<) f rr = f <<< toJSON rr <<< "\n"

readResults :: !*File -> *(! ?[RunResult], !*File)
readResults f
# (e,f) = fend f
| e = (?Just [], f)
# (l,f) = freadline f
# rr = fromJSON $ fromString l
| isNone rr = (?None, f)
# (rrs,f) = readResults f
= ((\rrs -> [fromJust rr:rrs]) <$> rrs, f)

mergeResults :: ![RunResult] -> [RunResult]
mergeResults [] = []
mergeResults [rr:rrs] = case partition (\rr` -> rr.run.TestRun.name == rr`.run.TestRun.name) rrs of
	([], rrs)    -> [rr:mergeResults rrs]
	([rr`], rrs) -> [{rr & result=merge rr.result rr`.result}:mergeResults rrs]
where
	merge :: !EndEventType !EndEventType -> EndEventType
	merge (Failed (?Just r1)) (Failed (?Just r2)) = case (r1,r2) of
		(FailedChildren cs1, FailedChildren cs2) -> Failed $ ?Just $ FailedChildren $ cs1 ++ cs2
	merge (Failed r) _ = Failed r
	merge _ (Failed r) = Failed r
	merge Skipped _       = Skipped
	merge _       Skipped = Skipped
	merge Passed  Passed  = Passed

derive gDiff GenConsAssoc, JSONNode
derive gPrint Expression, JSONNode

gDiff{|Expression|} x y = case (x,y) of
	(JSON x,   JSON y)   -> gDiff{|*|} x y
	(GPrint x, GPrint y) -> gDiff{|*|} (preParseString x) (preParseString y)
	_                    -> simpleDiff (printToString x) (printToString y)

gDiff{|Expr|} x y = case (x,y) of
	(ExprInt a,    ExprInt b)    -> gDiff{|*|} a b
	(ExprChar a,   ExprChar b)   -> gDiff{|*|} a b
	(ExprBool a,   ExprBool b)   -> gDiff{|*|} a b
	(ExprReal a,   ExprReal b)   -> gDiff{|*|} a b
	(ExprString a, ExprString b) -> gDiff{|*|} a b
	(ExprApp a, ExprApp b) | a.[0] == b.[0] -> [
			{ status   = parentStatus argDiffs
			, value    = toString a.[0]
			, children = argDiffs
			}]
	where
		argDiffs = flatten [gDiff{|*|} x y \\ x <-: a & y <-: b & n <- [0..] | n > 0]
	(ExprTuple a,  ExprTuple b)  -> [ {diff & value="_Tuple"}
	                                \\ diff <- gDiff{|*->*|} gDiff{|*|} [x \\ x <-: a] [y \\ y <-: b]
	                                ]
	(ExprList a,   ExprList b)   -> gDiff{|*|} a b
	(ExprArray a,  ExprArray b)  -> [ {diff & value="_Array"}
	                                \\ diff <- gDiff{|*->*|} gDiff{|*|} [x \\ x <- a] [y \\ y <- b]
	                                ]
	(ExprRecord r1 xs, ExprRecord r2 ys) | r1 == r2 -> [
			{ status   = parentStatus field_diffs
			, value    = fromMaybe "<unnamed record>" r1
			, children = field_diffs
			}]
		with
			field_diffs =
				[ let ds = gDiff{|*|} (find k xs) (find k ys) in
					{status=parentStatus ds, value=k +++ "=", children=ds} \\ k <- both] ++
				[{status=OnlyLeft,  value=k +++ "=", children=[{status=OnlyLeft,  value=toString (find k xs), children=[]}]} \\ k <- xonly] ++
				[{status=OnlyRight, value=k +++ "=", children=[{status=OnlyRight, value=toString (find k ys), children=[]}]} \\ k <- yonly]

			xkeys = [k \\ ExprField k _ <-: xs]
			ykeys = [k \\ ExprField k _ <-: ys]
			both = intersect xkeys ykeys
			xonly = difference xkeys ykeys
			yonly = difference ykeys xkeys
			find k vs = case [e \\ ExprField f e <-: vs | f == k] of
				[e:_] -> e
				_     -> abort "gDiff_Expr: internal error\n"
	_ -> simpleDiff (toString x) (toString y)

parentStatus :: [Diff] -> DiffStatus
parentStatus diffs = if (all (\d -> d.status == Common) diffs) Common Changed

simpleDiff :: !String !String -> [Diff]
simpleDiff left right
	| left == right =  [ {status=Common,    value=left,  children=[]} ]
	| otherwise     =  [ {status=OnlyLeft,  value=left,  children=[]}
	                   , {status=OnlyRight, value=right, children=[]}
	                   ]
