implementation module Testing.JUnitExport

import Control.Applicative
import Control.Monad
import Data.Func
import Data.Functor
import Data.GenDiff
import Data.Maybe
import StdEnv
import System.FilePath
import Testing.Options
import Testing.TestEvents
import qualified Text
from Text import class Text, instance Text String
import Text.HTML
import Text.Language

import Testing.Util

resultsToJUnitExport :: ![RunResult] -> JUnitExport
resultsToJUnitExport results
# suites = map resultsToSuite results
=
	{ jue_tests    = sum [s.jus_tests \\ s <- suites]
	, jue_failures = sum [s.jus_failures \\ s <- suites]
	, jue_skipped  = sum [s.jus_skipped \\ s <- suites]
	, jue_suites   = suites
	, jue_time     = sum [s.jus_time \\ s <- suites]
	}
where
	resultsToSuite :: !RunResult -> JUnitSuite
	resultsToSuite res =
		{ jus_name       = res.run.TestRun.name
		, jus_tests      = max 1 (length res.RunResult.children)
		, jus_failures   = sum [1 \\ {event=Failed _} <- res.RunResult.children]
		, jus_skipped    = sum [1 \\ {event=Skipped} <- res.RunResult.children]
		, jus_time       = res.RunResult.time
		, jus_cases      = map eventToCase res.RunResult.children
		}

	eventToCase :: !EndEvent -> JUnitCase
	eventToCase event =
		{ juc_id        = event.EndEvent.name
		, juc_name      = event.EndEvent.name
		, juc_classname = fromMaybe "unknown module" $ (\l -> l.moduleName) =<< event.EndEvent.location
		, juc_time      = fromMaybe 0 event.EndEvent.time
		, juc_failure   = case event.event of
			Failed ?None     -> ?Just "unknown reason"
			Failed (?Just r) -> ?Just (toString r)
			_                -> ?None
		, juc_skipped   = case event.event of
			Skipped -> ?Just ""
			_       -> ?None
		}

instance toString FailReason
where
	toString r = case r of
		FailedAssertions fas
			-> pluralisen English (length fas) "Failed assertion" +++ ":\n\n" +++ 'Text'.join "\n\n" [toString fa \\ fa <- fas]
		CounterExamples ces
			-> pluralisen English (length ces) "Counterexample" +++ ":\n\n" +++ 'Text'.join "\n\n" [toString ce \\ ce <- ces]
		FailedChildren fcs
			-> pluralisen English (length fcs) "Failed child test" +++ ":\n- " +++ 'Text'.join "\n- "
				[name +++ ": " +++ short reason \\ (name,reason) <- fcs]
			with
				short ?None     = "no reason given"
				short (?Just r) = case r of
					FailedAssertions fas -> pluralisen English (length fas) "failed assertion"
					CounterExamples ces  -> pluralisen English (length ces) "counterexample"
					FailedChildren fcs   -> pluralisen English (length fcs) "failed child test"
					Crashed              -> "crashed"
					CustomFailReason r   -> r
		Crashed
			-> "Crashed"
		CustomFailReason reason
			-> reason

instance toString FailedAssertion
where
	toString (ExpectedRelation a r b) = "\tExpected " +++ toString r +++ " on:\n" +++
		"\t- " +++ toString a +++ "\n" +++
		"\t- " +++ toString b +++
		case r of
			Eq -> "\n\tDiff:\n\t" +++ cleanDiff (diffToConsole (gDiff{|*|} a b))
			_  -> ""
	where
		cleanDiff =
			// Add indent
			'Text'.replaceSubString "\n" "\n\t" o
			// remove < and > because they are treated specially in XML
			(\s -> case s.[0] of
				'<' -> {if (i==0) '-' c \\ c <-: s & i <- [0..]}
				'>' -> {if (i==0) '+' c \\ c <-: s & i <- [0..]}
				_   -> s) o
			'Text'.replaceSubString "\n<" "\n-" o
			'Text'.replaceSubString "\n>" "\n+" o
			// remove ANSI colours because we're not printing to the console
			'Text'.replaceSubString "\033[0m" "" o
			'Text'.replaceSubString "\033[0;33m" "" o
			'Text'.replaceSubString "\033[0;32m" "" o
			'Text'.replaceSubString "\033[0;31m" ""

instance toString CounterExample
where
	toString ce = "Arguments:\n\t" +++
		'Text'.join "\n\t" [toString arg \\ arg <- ce.counterExample] +++
		case ce.failedAssertions of
			[]  -> ""
			fas -> "\n" +++ pluralisen English (length fas) "Failed assertion" +++ ":\n" +++
				'Text'.join "\n" ['Text'.replaceSubString "\n\t" "\n\t\t" (toString fa) \\ fa <- fas]

instance <<< JUnitExport
where
	(<<<) f jue
	# f = f <<< "<?xml version=\"1.0\"?>\n"
	# f = f <<< "<testsuites tests=\"" <<< jue.jue_tests
		<<< "\" failures=\"" <<< jue.jue_failures
		<<< "\" skipped=\"" <<< jue.jue_skipped
		<<< "\" time=\"" <<< time jue.jue_time
		<<< "\">\n"
	# f = seqSt printTestSuite jue.jue_suites f
	# f = f <<< "</testsuites>\n"
	= f
	where
		time :: !Int -> String
		time ms = toString (ms/1000) +++ "." +++ 'Text'.lpad (toString (ms rem 1000)) 3 '0'

		printTestSuite :: !JUnitSuite !*File -> *File
		printTestSuite jus f
		# f = f <<< "\t<testsuite name=\"" <<< escapeForAttribute jus.jus_name
			<<< "\" tests=\"" <<< jus.jus_tests
			<<< "\" failures=\"" <<< jus.jus_failures
			<<< "\" skipped=\"" <<< jus.jus_skipped
			<<< "\" time=\"" <<< time jus.jus_time
			<<< "\">\n"
		# f = seqSt printTestCase jus.jus_cases f
		# f = f <<< "\t</testsuite>\n"
		= f

		printProperty :: !(!String,!String) !*File -> *File
		printProperty (key,val) f = f <<< "\t\t\t<property name=\"" <<< escapeForAttribute key
			<<< "\" value=\"" <<< escapeForAttribute val
			<<< "\"?>\n"

		printTestCase :: !JUnitCase !*File -> *File
		printTestCase juc f
		# f = f <<< "\t\t<testcase id=\"" <<< escapeForAttribute juc.juc_id
			<<< "\" name=\"" <<< escapeForAttribute juc.juc_name
			<<< "\" classname=\"" <<< escapeForAttribute juc.juc_classname
			<<< "\" time=\"" <<< time juc.juc_time
			<<< "\">"
		# f = case juc.juc_failure of
			?None   -> f
			?Just r -> f <<< "\n\t\t\t<failure>" <<< escapeForTextNode r <<< "</failure>\n\t\t"
		# f = case juc.juc_skipped of
			?None   -> f
			?Just r -> f <<< "\n\t\t\t<skipped>" <<< escapeForTextNode r <<< "</skipped>\n\t\t"
		# f = f <<< "</testcase>\n"
		= f
