definition module Testing.Util

from StdFile import class <<<

from Data.GenDiff import :: Diff, generic gDiff
from Testing.Options import :: TestRun
from Testing.TestEvents import :: EndEvent, :: EndEventType, :: Expression
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

:: RunResult =
	{ run      :: !TestRun
	, result   :: !EndEventType
	, children :: ![EndEvent]
	, time     :: !Int //* Time in milliseconds
	}

derive JSONEncode RunResult
derive JSONDecode RunResult
instance <<< RunResult

readResults :: !*File -> *(! ?[RunResult], !*File)

mergeResults :: ![RunResult] -> [RunResult]

derive gDiff Expression
