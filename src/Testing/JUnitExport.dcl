definition module Testing.JUnitExport

from StdFile import class <<<
from System.FilePath import :: FilePath

from Testing.Util import :: RunResult

// See https://github.com/windyroad/JUnit-Schema for the full schema.
// This representation does not contain all fields.

:: JUnitExport =
	{ jue_tests    :: !Int
	, jue_failures :: !Int
	, jue_skipped  :: !Int
	, jue_suites   :: ![JUnitSuite]
	, jue_time     :: !Int // in milliseconds
	}

:: JUnitSuite =
	{ jus_name       :: !String
	, jus_tests      :: !Int
	, jus_failures   :: !Int
	, jus_skipped    :: !Int
	, jus_time       :: !Int // in milliseconds
	, jus_cases      :: ![JUnitCase]
	}

:: JUnitCase =
	{ juc_id        :: !String
	, juc_name      :: !String
	, juc_classname :: !String
	, juc_time      :: !Int // in milliseconds
	, juc_failure   :: !?String
	, juc_skipped   :: !?String
	}

resultsToJUnitExport :: ![RunResult] -> JUnitExport

instance <<< JUnitExport
