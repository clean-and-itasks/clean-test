definition module Util.ProcessOutput

:: *ProcessOutput =
	{ lines :: ![String]
	, rest  :: !String
	}

append :: !String !(String .e -> .e) !*ProcessOutput .e -> *(*ProcessOutput, .e)
