definition module Util.TestSelection

/**
 * @property-bootstrap
 *   import StdEnv
 *   import Data.Func
 *
 *   :: NrOfTests =: NrOfTests Int
 *   ggen{|NrOfTests|} _ = [!NrOfTests i \\ i <- [0..]]
 *   derive genShow NrOfTests
 *   derive gPrint NrOfTests
 *
 *   :: NrOfParallelJobs =: NrOfParallelJobs Int
 *   ggen{|NrOfParallelJobs|} _ = [!NrOfParallelJobs i \\ i <- [1..10]]
 *   derive genShow NrOfParallelJobs
 *   derive gPrint NrOfParallelJobs
 */

/** The selected tests for this batch, given idx and total, is every total_th element, with an offset of ids.
 * idx and total correspond to arguments of command-line option --parallel-job idx/total.
 * Examples:
 * [a, b, c, d, e, f, g] with idx 1 and total 3 = [a, d, g]
 * [a, b, c, d, e, f, g] with idx 2 and total 3 = [b, e]
 * [a, b, c, d, e, f, g] with idx 3 and total 3 = [c, f]
 *
 * A special case is when total is equal to or larger than the number of tests. In this case the selection
 * is simply the idx_th element.
 * Examples:
 * [a, b, c] with idx 1 and total 4 = [a]
 * [a, b, c] with idx 2 and total 4 = [b]
 * [a, b, c] with idx 3 and total 4 = [c]
 * [a, b, c] with idx 4 and total 4 = []
 * @param idx.
 * @param total.
 * @param All tests.
 * @result A test selection.
*/

/**
 * @property tests are correctly distributed among multiple jobs: A.nrOfTests :: NrOfTests; nrOfParallelJobs :: NrOfParallelJobs:
 *     (sort $ flatten $ allTestBatches nrOfParallelJobsInt tests) =.= tests
 *     where
 *         allTestBatches total tests = [pickTestsForParallelJob i total tests \\ i <- [1..total]]
 *         tests = [0..nrOfTestsInt]
 *         (NrOfTests nrOfTestsInt) = nrOfTests
 *         (NrOfParallelJobs nrOfParallelJobsInt) = nrOfParallelJobs
 */
pickTestsForParallelJob :: !Int !Int ![a] -> [a]