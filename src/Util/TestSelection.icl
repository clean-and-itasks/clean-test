implementation module Util.TestSelection

import StdEnv
import Data.List
import Data.Maybe
import Data.Func

pickTestsForParallelJob :: !Int !Int ![a] -> [a]
pickTestsForParallelJob idx total tests
	| length tests < total = maybe [] (\x -> [x]) (tests !? (idx - 1))
	= takeModulo total $ drop (idx - 1) tests

/* Takes every nth element.
 * @param n.
 * @param The list.
 * @result A list containing the first element, and then every nth element.
 */
takeModulo :: !Int ![a] -> [a]
takeModulo m rs = takeModulo` m 1 rs
where
	takeModulo` :: !Int !Int ![a] -> [a]
	takeModulo` _ _ [] = []
	takeModulo` m i [r:rs]
		| i == 1 = [r:takeModulo` m nextI rs]
		| otherwise = takeModulo` m nextI rs
		where
			nextI = if (i == m) 1 (inc i)