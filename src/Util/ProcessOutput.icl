implementation module Util.ProcessOutput

import StdEnv

import Data.Func
from Text import class Text(split), instance Text String

append :: !String !(String .e -> .e) !*ProcessOutput .e -> *(*ProcessOutput, .e)
append s f out env
| size s == 0 = (out, env)
# out & rest = out.rest +++ s
# lines = split "\n" out.rest
| length lines == 1 = (out, env)
# env = seqSt f (init lines) env
# out & lines = out.lines ++ init lines
# out & rest  = last lines
= (out, env)
