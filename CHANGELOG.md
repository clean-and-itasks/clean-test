#### 2.0.6

- Chore: upgrade system dependency.
- Chore: upgrade generic-print dependency.

#### 2.0.5

- Fix: issues with processing command line options.

#### 2.0.4

- Chore: support base 2.0.

#### 2.0.3

- Enhancement: do not shorten lines when printing undecodable client output.

#### 2.0.2

- Chore: accept clean-platform ^v0.3 and ^v0.4 as dependency.

#### 2.0.1

- Fix: update platform/property-tester versions for windows fixes to processes/files/system environments.

## 2.0.0
Initial version.
